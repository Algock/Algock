from yahoo_finance import Share
from pyodbc import connect
from requests import get
from html2text import html2text
from dateutil.parser import parse


def load_stock_symbols(stocks_names_file):
    stocks_symbols = []
    for row in stocks_names_file.readlines():
        stock_data = {row.split("|")[0]: row.split("|")[1].lower()}
        print stock_data
        stocks_symbols.append(stock_data)
    return stocks_symbols


def insert_to_stocks_table(selected_stock, start_date, end_date, cursor):
    database_name = "dbo"
    stock_table_name = "Stocks"
    stock_table_full_name = "{}.{}".format(database_name, stock_table_name)
    query = "INSERT INTO {} VALUES ".format(stock_table_full_name)

    print selected_stock[0]
    temp_stock = Share(selected_stock[0])

    stock_historical_data = temp_stock.get_historical(start_date, end_date)

    i = 0
    for stock_data_per_day in stock_historical_data:
        if i < 999:
            columns_stock = "({},{},{},{},{},{},{},{},{}), ".format("'{}'".format(selected_stock[1]),
                                                                    "'{}'".format(selected_stock[0]),
                                                                    "'{}'".format(stock_data_per_day['Date']),
                                                                    stock_data_per_day['Open'],
                                                                    stock_data_per_day['High'],
                                                                    stock_data_per_day['Low'],
                                                                    stock_data_per_day['Close'],
                                                                    stock_data_per_day['Volume'],
                                                                    stock_data_per_day['Adj_Close'])
            query += columns_stock
            print columns_stock
            i += 1
        else:
            query = query[:-2]
            print query
            cursor.execute(query)
            cursor.commit()
            query = "INSERT INTO {} VALUES ".format(stock_table_full_name)
            i = 0
    if i > 0:
        query = query[:-2]
        print query
        cursor.execute(query)
        cursor.commit()


def insert_to_stocks_splits_table(selected_stock, cursor):
    database_name = "dbo"
    stock_splits_table_name = "StocksSplits"
    stock_splits_table_full_name = "{}.{}".format(database_name, stock_splits_table_name)
    query = "INSERT INTO {} VALUES ".format(stock_splits_table_full_name)

    r = get('http://getsplithistory.com/{}'.format(selected_stock[0]))
    table_word_index_start = r.text.index("<tbody>")
    table_word_index_end = r.text.index("</tbody>")
    split_table = r.text[table_word_index_start:table_word_index_end]

    splits = html2text(split_table).split("\n")
    new_splits = [split for split in splits if split != '']
    new_splits.pop()

    i = 0
    for split in new_splits[:]:
        data = split.split("|")
        date = str(parse(data[0])).split(" ")[0]
        ratio = data[1].split(" : ")[0]

        if i < 999:
            columns_stock_split = "({},{},{}), ".format("'{}'".format(selected_stock[0]), "'{}'".format(date), ratio)
            query += columns_stock_split
            print columns_stock_split
            i += 1
        else:
            query = query[:-2]
            print query
            cursor.execute(query)
            cursor.commit()
            query = "INSERT INTO {} VALUES ".format(stock_splits_table_full_name)
            i = 0
    if i > 0:
        query = query[:-2]
        print query
        cursor.execute(query)
        cursor.commit()


def insert_stock_data_into_db(selected_stocks_tuple, start_date, end_date, cursor):
    for selected_stock in selected_stocks_tuple:
        insert_to_stocks_table(selected_stock, start_date, end_date, cursor)
        insert_to_stocks_splits_table(selected_stock, cursor)

conn = connect(
    r'DRIVER={ODBC Driver 13 for SQL Server};'
    r'SERVER=algock-dev01.database.windows.net;'
    r'DATABASE=AlgockDB;'
    r'UID=Algock;'
    r'PWD=Moti1234'
)
cursor = conn.cursor()

stocks_names_file = open("C:\\Users\\user\\Desktop\\StocksNames.csv", "rb")
# stocks_symbols = load_stock_symbols(stocks_names_file)

# selected_stocks = ["YHOO", "MSFT", "NFLX", "MRVL", "TSLA", "FORD"]
selected_stocks_tuple = [["MRVL", "Marvel"]]
start_date = '2012-03-01'
end_date = '2017-03-01'


insert_stock_data_into_db(selected_stocks_tuple, start_date, end_date, cursor)
