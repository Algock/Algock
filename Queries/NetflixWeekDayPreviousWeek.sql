-- Joins a Stock value with its related google trends popularity to use for a 
-- Parameter A: The difference between the Close value of a stock on a given day of the week and the average Close value of a stock in the previous week
-- Parameter B: The difference between the Percentage values of the previous week and its previous
-- ====================================================================================================================================

-- The symbol of the requested stock
declare @stockSymbol nvarchar(MAX)
-- The keyword to check on google trends
declare @keyword nvarchar(MAX)
-- The amount of days to check between a week's google terms percentage and the stock value
declare @daysAfterSunday int
-- The threshold for the google trends percentage difference
declare @percentageDiffThreshold int
-- The threshold for the stock value difference
declare @calculatedDiffThreshold int

set @stockSymbol = 'NFLX'
set @keyword = 'netflix new series'
set @daysAfterSunday = 8 -- 8 is the following week's Monday
set @percentageDiffThreshold = 2
set @calculatedDiffThreshold = 0

SELECT * FROM 
	(SELECT s.[Name]
		   ,s.[Symbol]
		   ,t.[Keyword]
		   ,@daysAfterSunday as DaysAfterSunday
		   ,@percentageDiffThreshold as PercentageDiffThreshold
		   ,@calculatedDiffThreshold as CalculatedDiffThreshold
		   ,s.[Date] as StockDate
		   ,s.[Open]
		   ,s.[High]
		   ,s.[Low]
		   ,s.[Close]
		   ,s.[Volume]
		   ,s.[AdjClose]
		   ,t.[Date] as TermDate
		   ,t.[Percentage] as SelfPercentage
		   ,tn.[Percentage] as NormalizedPercentage
		   ,tn.[Percentage] - (SELECT tnlast.[Percentage] 
			  				   FROM [dbo].[TermsNormalize] tnlast WHERE tnlast.[StockSymbol] = s.[Symbol] and tnlast.[Date] = DATEADD(day,-7,tn.[Date]) and tnlast.[Keyword] = @keyword) as NormalizedPercentageDiff
		   ,t.[Percentage] - (SELECT tlast.[Percentage] 
							  FROM [dbo].[Terms] tlast WHERE tlast.[StockSymbol] = s.[Symbol] and tlast.[Date] = DATEADD(day,-7,tn.[Date]) and tlast.[Keyword] = @keyword) as PercentageDiff						  
		   ,s.[Close] - s.[Open] as Diff
		   ,s.[Close] - (SELECT AVG(st.[Close]) FROM [dbo].[Stocks] st WHERE st.[Symbol] = s.[Symbol] and st.[Date] between DATEADD(day, -7, s.[Date]) and s.[Date]) as CalculatedDiff 
	  FROM [dbo].[Stocks] s, [dbo].[Terms] t, [dbo].[TermsNormalize] tn
	  WHERE s.[Symbol] = @stockSymbol and 
		t.[Keyword] = @keyword and
		s.[Symbol] = t.[StockSymbol] and
		s.[Symbol] = tn.[StockSymbol] and
		t.[Date] = tn.[Date] and
		t.[Keyword] = tn.[Keyword] and
		s.[Date] = DATEADD(day,@daysAfterSunday,t.[Date])) a
WHERE a.[PercentageDiff] IS NOT NULL and 
	ABS(a.[PercentageDiff]) >= @percentageDiffThreshold and 
	a.CalculatedDiff > @calculatedDiffThreshold